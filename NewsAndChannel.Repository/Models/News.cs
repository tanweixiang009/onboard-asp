﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NewsAndChannel.Repository.Models
{
    public class News
    {
        [Key]
        public int ID { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public CommonStatus Status { get; set; }
       
    }
}