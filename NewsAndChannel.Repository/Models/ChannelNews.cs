﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Repository.Models
{
    public class ChannelNews
    {
        [Key]
        public int ID { get; set; }
        public int ChannelID { get; set; }
        public int NewsID { get; set; }
    }
}