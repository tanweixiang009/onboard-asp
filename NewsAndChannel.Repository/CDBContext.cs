﻿using Microsoft.AspNet.Identity.EntityFramework;
using NewsAndChannel.Repository.Models;
using System.Data.Entity;

namespace NewsAndChannel.Repository
{
    public class CDBContext : IdentityDbContext<ApplicationUser>//DbContext
    {
        public CDBContext() : base("StringDBContext")
        { }

        public DbSet<News> News { get; set; }

        public DbSet<Channel> Channel { get; set; }

        public DbSet<ChannelNews> ChannelNews { get; set; }

        
    }
}