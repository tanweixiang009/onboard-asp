namespace NewsAndChannel.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyMigration3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.News", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.News", "Title", c => c.String());
        }
    }
}
