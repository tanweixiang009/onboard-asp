namespace NewsAndChannel.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyMigration31 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Channels", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Channels", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.Channels", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.News", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.News", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.News", "Title", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.News", "Title", c => c.String(nullable: false));
            DropColumn("dbo.News", "Status");
            DropColumn("dbo.News", "UpdatedDate");
            DropColumn("dbo.Channels", "Status");
            DropColumn("dbo.Channels", "UpdatedDate");
            DropColumn("dbo.Channels", "CreatedDate");
        }
    }
}
