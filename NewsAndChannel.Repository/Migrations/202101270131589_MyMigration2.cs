namespace NewsAndChannel.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyMigration2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ChannelNews", "ChannelID", c => c.Int(nullable: false));
            AlterColumn("dbo.ChannelNews", "NewsID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ChannelNews", "NewsID", c => c.String());
            AlterColumn("dbo.ChannelNews", "ChannelID", c => c.String());
        }
    }
}
