﻿using NewsAndChannel.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Api.Helper
{
    public class WebApiWrapper
    {
        public delegate T GenericWebApi<T>(params object[] inputs);

        public static TResult Call<TResult>(GenericWebApi<TResult> func, params object[] inputs)
        {
            try
            {
                return func(inputs);
            }
            catch (Exception ex)
            {
                if (ex.InnerException is WebApiException)
                {
                    throw ex.InnerException;
                }

                if (ex is WebApiException)
                {
                    throw;
                }
                else
                {
                    throw new WebApiException(new Models.Result(ex));
                }
            }
        }
    }
}