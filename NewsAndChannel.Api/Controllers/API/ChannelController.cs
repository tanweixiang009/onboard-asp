﻿using NewsAndChannel.Api.Helper;
using NewsAndChannel.Service.Generic;
using NewsAndChannel.Service.Models;
using NewsAndChannel.Service.Services;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NewsAndChannel.Api.Controllers.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Channel")]
    public class ChannelController : ApiController
    {

        public readonly ChannelService _ChannelService;

        public ChannelController()
        {
            _ChannelService = new ChannelService();
        }

        [HttpPost]
        [Route("v1/getChannelList")]
        public IHttpActionResult GetChannelList(GeneralListingRequestModel model)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelService.GetChannelList(model)));

        }

        [HttpGet]
        [Route("v1/getChannel")]
        public IHttpActionResult GetChannel(int ChannelId)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelService.GetChannel(ChannelId)));

        }

        [HttpPost]
        [Route("v1/submitChannel")]
        public IHttpActionResult SubmitChannel(ChannelModel model)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelService.SubmitChannel(model)));

        }

        [HttpGet]
        [Route("v1/deleteChannel")]
        public IHttpActionResult DeleteChannel(int ChannelId)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelService.DeleteChannel(ChannelId)));
        }
    }
}
