﻿using NewsAndChannel.Api.Helper;
using NewsAndChannel.Service.Models;
using NewsAndChannel.Service.Services;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NewsAndChannel.Api.Controllers.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/ChannelNews")]
    public class ChannelNewsController : ApiController
    {
        public readonly ChannelNewsService _ChannelNewsService;

        public ChannelNewsController()
        {
            _ChannelNewsService = new ChannelNewsService();
        }

        [HttpGet]
        [Route("v1/getSelectedNewsListByChannelId")]
        public IHttpActionResult GetNewsListByChannelId(int ChannelId)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelNewsService.GetSelectedNewsListByChannelId(ChannelId)));

        }

        [HttpGet]
        [Route("v1/getUnSelectedNewsListByChannelId")]
        public IHttpActionResult GetUnSelectedNewsListByChannelId(int ChannelId)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelNewsService.GetUnSelectedNewsListByChannelId(ChannelId)));

        }

        [HttpGet]
        [Route("v1/getSelectedChannelListByNewsId")]
        public IHttpActionResult GetSelectedChannelListByNewsId(int NewsId)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelNewsService.GetSelectedChannelListByNewsId(NewsId)));

        }

        [HttpGet]
        [Route("v1/getUnSelectedChannelListByNewsId")]
        public IHttpActionResult GetUnSelectedChannelListByNewsId(int NewsId)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelNewsService.GetUnSelectedChannelListByNewsId(NewsId)));

        }

        [HttpPost]
        [Route("v1/submitChannelNews")]
        public IHttpActionResult SubmitChannelNews(ChannelNewsModel model)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelNewsService.SubmitChannelNews(model)));

        }

        [HttpPost]
        [Route("v1/submitChannelNewsList")]
        public IHttpActionResult SubmitChannelNewsList(List<ChannelNewsModel> model)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelNewsService.SubmitChannelNewsList(model)));

        }

        [HttpDelete]
        [Route("v1/deleteChannelNews")]
        public IHttpActionResult DeleteChannelNews(ChannelNewsModel model)
        {
            return Ok(WebApiWrapper.Call(e => _ChannelNewsService.DeleteChannelNews(model)));

        }
    }
}
