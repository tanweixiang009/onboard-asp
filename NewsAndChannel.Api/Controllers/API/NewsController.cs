﻿
using NewsAndChannel.Api.Helper;
using NewsAndChannel.Service.Generic;
using NewsAndChannel.Service.Models;
using NewsAndChannel.Service.Services;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NewsAndChannel.Api.Controllers.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/News")]
    public class NewsController : ApiController
    {
        public readonly NewsService _NewsService;

        public NewsController()
        {
            _NewsService = new NewsService();
        }

        [HttpPost]
        [Route("v1/getNewsList")]
        public IHttpActionResult GetNewsList(GeneralListingRequestModel model)
        {
            return Ok(WebApiWrapper.Call(e => _NewsService.GetNewsList(model)));

        }

        [HttpGet]
        [Route("v1/getNews")]
        public IHttpActionResult GetNews(int NewsId)
        {
            return Ok(WebApiWrapper.Call(e => _NewsService.GetNews(NewsId)));

        }

        [HttpPost]
        [Route("v1/submitNews")]
        public IHttpActionResult SubmitNews(NewsModel model)
        {
            return Ok(WebApiWrapper.Call(e => _NewsService.SubmitNews(model)));

        }

        [HttpGet]
        [Route("v1/deleteNews")]
        public IHttpActionResult DeleteNews(int NewsId)
        {
            return Ok(WebApiWrapper.Call(e => _NewsService.DeleteNews(NewsId)));

        }
    }
}
