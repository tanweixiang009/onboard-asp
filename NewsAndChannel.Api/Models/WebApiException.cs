﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Api.Models
{
    public class WebApiException : System.Web.Http.HttpResponseException
    {

        public Result ErrorDetails { get; set; }

        public WebApiException(Result webApiError)
            : base(webApiError.HttpResponseMessage)
        {
            ErrorDetails = webApiError;         
        }

    }
}