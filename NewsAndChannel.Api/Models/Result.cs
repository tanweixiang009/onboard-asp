﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Api.Models
{
    public class Result
    {
        public ServiceStatus Status { get; set; }
        public object Data { get; set; }
        public System.Net.Http.HttpResponseMessage HttpResponseMessage { get; set; }

        public Result()
        {
        }

        public Result(Exception exception)
        {             

            Status = new ServiceStatus(StandardMessage.ERROR, exception);
            string JsonString = this.ToJsonString().ToLower();

            HttpResponseMessage = new System.Net.Http.HttpResponseMessage()
            {
                Content = new System.Net.Http.StringContent(JsonString),
                ReasonPhrase = "OK"//exception.Message.ToString()   // Due to the logic is no matter how also is 200 OK.
            };
            HttpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

        }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
        }
    }
}