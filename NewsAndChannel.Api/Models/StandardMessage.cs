﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Api.Models
{
    public class StandardMessage : EnumBaseType<StandardMessage>
    {
        public StandardMessage(int code, string name, string value) : base(code, name, value) { }

        public static StandardMessage GetByKey(int key) { return GetBaseByKey(key); }


        //GENERAL
        #region
        public static readonly StandardMessage OK = new StandardMessage(200, "OK", "OK");
        public static readonly StandardMessage Continue = new StandardMessage(100, "Continue", "Continue");
        public static readonly StandardMessage ERROR = new StandardMessage(404, "Error", "Error");
        #endregion

    }
}