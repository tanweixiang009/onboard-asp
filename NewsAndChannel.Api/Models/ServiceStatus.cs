﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Api.Models
{
    public class ServiceStatus
    {
        public int Code { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }

        public ServiceStatus(StandardMessage msg)
        {
            Code = msg.Code;
            Title = msg.Name;
            Message = msg.Value;
        }
        public ServiceStatus(StandardMessage msg, string error)
        {
            Code = msg.Code;
            Title = msg.Name;
            Message = error;
        }
        public ServiceStatus(StandardMessage msg, Exception ex)
        {
            Code = msg.Code;
            Title = msg.Name;
            Message = ex.GetBaseException().Message;
        }
        public ServiceStatus(ExceptionMessage msg)
        {
            Code = msg.Code;
            Title = msg.Name;
            Message = msg.Value;
        }
        public ServiceStatus(ExceptionMessage msg, string error)
        {
            Code = msg.Code;
            Title = msg.Name;
            Message = error;
        }
        public ServiceStatus(ExceptionMessage msg, Exception ex)
        {
            Code = msg.Code;
            Title = msg.Name;
            Message = ex.InnerException.InnerException.Message;
        }
    }
}