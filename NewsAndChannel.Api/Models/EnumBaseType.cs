﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Api.Models
{
    public abstract class EnumBaseType<T> where T : EnumBaseType<T>
    {
        protected static List<T> enumValues = new List<T>();

        public readonly int Code;
        public readonly string Name;
        public readonly string Value;

        public EnumBaseType(int code, string name, string value)
        {
            Code = code;
            Name = name;
            Value = value;
            enumValues.Add((T)this);
        }

        protected static ReadOnlyCollection<T> GetBaseValues()
        {
            return enumValues.AsReadOnly();
        }

        protected static T GetBaseByKey(int key)
        {
            foreach (T t in enumValues)
            {
                if (t.Code == key) return t;
            }
            return null;
        }

        public override string ToString()
        {
            return Value;
        }

        public string GetName()
        {
            return Name;
        }
    }
}