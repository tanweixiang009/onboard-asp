﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Api.Models
{
    public class ExceptionMessage : EnumBaseType<ExceptionMessage>
    {
        public ExceptionMessage(int code, string name, string value) : base(code, name, value) { }

        public static ExceptionMessage GetByKey(int key) { return GetBaseByKey(key); }



        //GENERAL
        #region
        public static readonly ExceptionMessage ServiceDown = new ExceptionMessage(600, "EXCEPTION600", "Oops. Something went wrong. Please try again later");
        public static readonly ExceptionMessage ServiceFailed = new ExceptionMessage(401, "EXCEPTION401", "Oops. Something went wrong. Please try again later");
        public static readonly ExceptionMessage ServiceForbidden = new ExceptionMessage(403, "EXCEPTION403", "Forbidden");
        public static readonly ExceptionMessage IncompleteField = new ExceptionMessage(602, "EXCEPTION602", "Incomplete field(s).");

        public static readonly ExceptionMessage InvalidClientId = new ExceptionMessage(400, "Invalid Client Id", "Client Id Error");

        #endregion

        //PROFESSIONAL ACCOUNT 600-699
        public static readonly ExceptionMessage AdminEmailMissing_AccountCreation = new ExceptionMessage(600, "EXCEPTION600", "Caveat Professional Sub-account successfully created. Administrator’s email address is blank or invalid. Please go to My Profile > My Details to update the email address in order to receive system notifications or announcements.");
        public static readonly ExceptionMessage AdminEmailMissing_AccountUpdated = new ExceptionMessage(601, "EXCEPTION601", "Caveat Professional Sub-account updated successfully. Administrator’s email address is blank or invalid. Please go to My Profile > My Details to update the email address in order to receive system notifications or announcements.");
        public static readonly ExceptionMessage AdminEmailMissing_AccountDisabled = new ExceptionMessage(602, "EXCEPTION602", "Caveat Professional Sub-account removed successfully. Administrator’s email address is blank or invalid. Please go to My Profile > My Details to update the email address in order to receive system notifications or announcements.");

        //SSO 700-799
        #region
        public static readonly ExceptionMessage MemberNotFound = new ExceptionMessage(700, "EXCEPTION700", "Failed: Member not found. If you have problem with account login, please contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");
        public static readonly ExceptionMessage MemberIncorrectAuth = new ExceptionMessage(701, "EXCEPTION701", "Failed: Incorrect username or password. If you have problem with account login, please contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");
        public static readonly ExceptionMessage DuplicateAccount = new ExceptionMessage(721, "EXCEPTION721", "Failed: Existing account found.");
        public static readonly ExceptionMessage DuplicateEmail = new ExceptionMessage(722, "EXCEPTION722", "Failed: Existing email found.");
        public static readonly ExceptionMessage DuplicateMobileNo = new ExceptionMessage(723, "EXCEPTION723", "Failed: Existing mobile no. found.");
        #endregion

        //SUBSCRIPTION 800-899
        #region
        public static readonly ExceptionMessage NoSubscription = new ExceptionMessage(800, "NO SUBSCRIPTION", "Sorry, your subscription has expired or your existing subscription does not include this option. To subscribe or renew this service, please contact SISVREALink sales rep at 6424 0277 or email us at sales@sisvrealink.com, or visit www.powerdesq.com to subscribe.");

        public static readonly ExceptionMessage CaveatSharedAccountFullyOccupied = new ExceptionMessage(801, "CAVEAT SHARED ACCOUNT FULLY OCCUPIED", "Sorry, all your caveat shared accounts have been fully occupied at the momment. Please try again later. If the problem persists, please contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");

        public static readonly ExceptionMessage CaveatPROLoginException = new ExceptionMessage(802, "CAVEAT PRO ACCOUNT LOGIN EXCEPTION", "Oops, there's something wrong with your account. Please contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");

        public static readonly ExceptionMessage AccountDisabled = new ExceptionMessage(803, "ACCOUNT DISABLED", "Oops, there's something wrong with your account. Please contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");

        public static readonly ExceptionMessage FirstTimeLogin = new ExceptionMessage(804, "FIRST TIME LOGIN", "Your account is not activated yet, please activate your account by clicking the [First Time Login]. If you have problem with the activation, please contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");

        public static readonly ExceptionMessage SubscriptionRegistrationExists = new ExceptionMessage(805, "Subscription Registration Exists", "Sorry, you have already register an account with us. Please login your account at web.powerdesq.com ,or check your mailbox for your account information. If you have problem with your account, please contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");

        public static readonly ExceptionMessage SubscriptionRegistrationDetailDuplicate = new ExceptionMessage(806, "Subscription Registration Detail Duplicate", "Sorry, we are unable to process your registration. Our support rep will you within 24 hours, or contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");

        public static readonly ExceptionMessage SubscriptionExtendedPending = new ExceptionMessage(807, "Subscription Extended", "Thank you for your support with SISVREALink. We have received your application. Our Sales personnel will get in touch with you within 3 working days, or contact SISVREALink support rep at 6424 0275/6424 0276 or email us at realinkcare@sisvrealink.com");

        #endregion
    }
}