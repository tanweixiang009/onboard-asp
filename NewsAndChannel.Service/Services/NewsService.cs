﻿using NewsAndChannel.Repository;
using NewsAndChannel.Repository.Models;
using NewsAndChannel.Service.Generic;
using NewsAndChannel.Service.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace NewsAndChannel.Service.Services
{
    public class NewsService
    {
        /// <summary>
        /// Get news listing
        /// </summary>
        /// <returns></returns>
        public GeneralListingResponseModel<NewsModel> GetNewsList(GeneralListingRequestModel model)
        {
            GeneralListingResponseModel<NewsModel> result = new GeneralListingResponseModel<NewsModel>();
            using (var db = new CDBContext())
            {
                #region null-handle

                if (string.IsNullOrEmpty(model.OrderDir))
                {
                    model.OrderDir = "asc";
                }
                else
                {
                    if (model.OrderDir.ToLower().Trim() != "asc" && model.OrderDir.ToLower().Trim() != "desc")
                    {
                        model.OrderDir = "asc";
                    }
                }
                if (model.PageSize == 0)
                {
                    model.PageSize = 10;
                }
                if (string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "";
                }


                #endregion

                //Get list
                var query = db.News.Where(e => e.Status == CommonStatus.Active
                    && (string.IsNullOrEmpty(model.SearchKeyword) || e.Title.Contains(model.SearchKeyword)))
                    .Select(e => new NewsModel()
                    {
                        ID = e.ID,
                        Title = e.Title,
                        Content = e.Content,
                        CreatedDate = e.CreatedDate,
                        UpdatedDate = e.UpdatedDate,
                        Status = e.Status
                    });
                switch (model.OrderDir.Trim().ToLower())
                {
                    case "asc":
                        switch (model.SortColumn.ToLower())
                        {
                            case "title":
                                query = query.OrderBy(e => e.Title);
                                break;
                            case "createddate":
                                query = query.OrderBy(e => e.CreatedDate);
                                break;
                            case "content":
                                query = query.OrderBy(e => e.Content);
                                break;
                            case "updateddate":
                                query = query.OrderBy(e => e.UpdatedDate);
                                break;
                            default:
                                query = query.OrderBy(e => e.CreatedDate);
                                break;
                        }
                        break;
                    case "desc":
                        switch (model.SortColumn.ToLower())
                        {
                            case "title":
                                query = query.OrderByDescending(e => e.Title);
                                break;
                            case "createddate":
                                query = query.OrderByDescending(e => e.CreatedDate);
                                break;
                            case "content":
                                query = query.OrderByDescending(e => e.Content);
                                break;
                            case "updateddate":
                                query = query.OrderByDescending(e => e.UpdatedDate);
                                break;
                            default:
                                query = query.OrderByDescending(e => e.CreatedDate);
                                break;
                        }
                        break;
                }
                GeneralListingResponseModel<NewsModel> data = new GeneralListingResponseModel<NewsModel>();
                data.TotalCount = query.Count();
                data.List = query.Skip(model.PageIndex * model.PageSize).Take(model.PageSize).ToList();

                result = data;
            }
            return result;
        }

        /// <summary>
        /// Get News record
        /// </summary>
        public NewsModel GetNews(int NewsId)
        {
            NewsModel result = new NewsModel();
            using (var db = new CDBContext())
            {
                #region null-handle
                if (NewsId == 0)
                {
                    throw new Exception("No Id specified");
                }

                #endregion

                //Get record
                var entityNews = db.News.Where(e => e.ID == NewsId && e.Status == CommonStatus.Active).FirstOrDefault();

                if (entityNews == null)
                {
                    throw new Exception("Record not found.");
                }
                result = new NewsModel(entityNews);
            }

            return result;

        }

        /// <summary>
        /// Create/Update News
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public GenericResponseModel SubmitNews(NewsModel model)
        {
            GenericResponseModel result = new GenericResponseModel();
            if (model.ID == 0)
            {
                #region Add New News
                using (var db = new CDBContext())
                {
                    #region null-handle    
                    if (string.IsNullOrEmpty(model.Title))
                    {
                        throw new Exception("Title is required");
                    }
                    if (string.IsNullOrEmpty(model.Content))
                    {
                        throw new Exception("Content is required");
                    }
                    #endregion

                    #region Create      
                    var news = new News()
                    {
                        Content = model.Content,
                        Title = model.Title,
                        CreatedDate = DateTime.Now,
                        Status = CommonStatus.Active
                    };

                    db.News.Add(news);
                    db.SaveChanges();
                    result = new GenericResponseModel();
                    result.Success = true;
                    result.Message = news.ID.ToString();
                    #endregion                
                }
                #endregion
            }
            else
            {
                #region Edit News
                using (var db = new CDBContext())
                {
                    #region null-handle
                    if (string.IsNullOrEmpty(model.Title))
                    {
                        throw new Exception("Title is required");
                    }
                    if (string.IsNullOrEmpty(model.Content))
                    {
                        throw new Exception("Content is required");
                    }

                    var entityNews = db.News.Where(e => e.ID == model.ID && e.Status == CommonStatus.Active).FirstOrDefault();
                    if (entityNews == null)
                    {
                        throw new Exception("Record not found");
                    }
                    #endregion

                    #region Update News   
                    bool hasUpdate = false;
                    if (model.Title != entityNews.Title)
                    {
                        entityNews.Title = model.Title;
                        hasUpdate = true;
                    }
                    if (model.Content != entityNews.Content)
                    {
                        entityNews.Content = model.Content;
                        hasUpdate = true;
                    }

                    if (hasUpdate)
                    {
                        entityNews.UpdatedDate = DateTime.Now;
                        db.SaveChanges();
                        result = new GenericResponseModel();
                        result.Success = true;
                    }
                    #endregion
                }
                #endregion
            }

            return result;
        }

        public GenericResponseModel DeleteNews(int id)
        {
            GenericResponseModel result = new GenericResponseModel();
            if (id == 0)
            {
                throw new Exception("Id is required");
            }

            using (var db = new CDBContext())
            {
                var news = db.News.Where(e => e.Status == CommonStatus.Active && e.ID == id).Select(e => e).FirstOrDefault();
                if (news == null)
                {
                    throw new Exception("Record not found");
                }
                news.Status = CommonStatus.Deleted;
                var query = db.ChannelNews.Where(e => e.NewsID == id);
                db.ChannelNews.RemoveRange(query);
                db.SaveChanges();
                result = new GenericResponseModel();
                result.Success = true;
                return result;
            }
        }
    }
}