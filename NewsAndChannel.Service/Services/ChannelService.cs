﻿using NewsAndChannel.Repository;
using NewsAndChannel.Repository.Models;
using NewsAndChannel.Service.Generic;
using NewsAndChannel.Service.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace NewsAndChannel.Service.Services
{
    public class ChannelService
    {
        public GeneralListingResponseModel<ChannelModel> GetChannelList(GeneralListingRequestModel model)
        {
            GeneralListingResponseModel<ChannelModel> result = new GeneralListingResponseModel<ChannelModel>();
            using (var db = new CDBContext())
            {
                #region null-handle
                if (string.IsNullOrEmpty(model.OrderDir))
                {
                    model.OrderDir = "asc";
                }
                else
                {
                    if (model.OrderDir.ToLower().Trim() != "asc" && model.OrderDir.ToLower().Trim() != "desc")
                    {
                        model.OrderDir = "asc";
                    }
                }
                if (model.PageSize == 0)
                {
                    model.PageSize = 10;
                }
                if (string.IsNullOrEmpty(model.SortColumn))
                {
                    model.SortColumn = "";
                }
                #endregion


                //Get list
                var query = db.Channel.Where(e => e.Status == CommonStatus.Active
                    && (string.IsNullOrEmpty(model.SearchKeyword) || e.Name.Contains(model.SearchKeyword)))
                    .Select(e => new ChannelModel()
                    {
                        ID = e.ID,
                        Name = e.Name,
                        Description = e.Description,
                        CreatedDate = e.CreatedDate,
                        UpdatedDate = e.UpdatedDate,
                        Status = e.Status
                    });
                switch (model.OrderDir.Trim().ToLower())
                {
                    case "asc":
                        switch (model.SortColumn.ToLower())
                        {
                            case "name":
                                query = query.OrderBy(e => e.Name);
                                break;
                            case "description":
                                query = query.OrderBy(e => e.Description);
                                break;
                            case "createddate":
                                query = query.OrderBy(e => e.CreatedDate);
                                break;
                            case "updateddate":
                                query = query.OrderBy(e => e.UpdatedDate);
                                break;
                            default:
                                query = query.OrderBy(e => e.CreatedDate);
                                break;
                        }
                        break;
                    case "desc":
                        switch (model.SortColumn.ToLower())
                        {
                            case "name":
                                query = query.OrderByDescending(e => e.Name);
                                break;
                            case "description":
                                query = query.OrderByDescending(e => e.Description);
                                break;
                            case "createddate":
                                query = query.OrderByDescending(e => e.CreatedDate);
                                break;
                            case "updateddate":
                                query = query.OrderByDescending(e => e.UpdatedDate);
                                break;
                            default:
                                query = query.OrderByDescending(e => e.CreatedDate);
                                break;
                        }
                        break;


                }


                GeneralListingResponseModel<ChannelModel> data = new GeneralListingResponseModel<ChannelModel>();
                data.TotalCount = query.Count();
                data.List = query.Skip(model.PageIndex * model.PageSize).Take(model.PageSize).ToList();

                result = data;

            }
            return result;
        }

        /// <summary>
        /// Get Channel record
        /// </summary>
        public ChannelModel GetChannel(int ChannelId)
        {
            ChannelModel result = new ChannelModel();
            using (var db = new CDBContext())
            {
                #region null-handle
                if (ChannelId == 0)
                {
                    throw new Exception("No Id specified");
                }

                #endregion

                //Get record
                var entityChannel = db.Channel.Where(e => e.ID == ChannelId && e.Status == CommonStatus.Active).FirstOrDefault();

                if (entityChannel == null)
                {
                    throw new Exception("Record not found.");
                }
                result = new ChannelModel(entityChannel);
            }
            return result;
        }

        /// <summary>
        /// Create/Update Channel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public GenericResponseModel SubmitChannel(ChannelModel model)
        {

            GenericResponseModel result = new GenericResponseModel();

            if (model.ID == 0)
            {
                #region Add New Channel
                using (var db = new CDBContext())
                {
                    #region null-handle    
                    if (string.IsNullOrEmpty(model.Name))
                    {
                        throw new Exception("Name is required");
                    }
                    if (string.IsNullOrEmpty(model.Description))
                    {
                        throw new Exception("Description is required");
                    }
                    #endregion

                    #region Create
                    Channel channel = new Channel()
                    {
                        Name = model.Name,
                        Description = model.Description,
                        CreatedDate = DateTime.Now,
                        Status = CommonStatus.Active

                    };

                    db.Channel.Add(channel);
                    db.SaveChanges();
                    result = new GenericResponseModel();
                    result.Success = true;
                    result.Message = channel.ID.ToString();
                    #endregion

                }
                #endregion
            }
            else
            {
                #region Edit Channel
                using (var db = new CDBContext())
                {
                    #region null-handle
                    if (string.IsNullOrEmpty(model.Name))
                    {
                        throw new Exception("Name is required");
                    }
                    if (string.IsNullOrEmpty(model.Description))
                    {
                        throw new Exception("Description is required");
                    }

                    var entityChannel = db.Channel.Where(e => e.ID == model.ID && e.Status == CommonStatus.Active).FirstOrDefault();
                    if (entityChannel == null)
                    {
                        throw new Exception("Record not found");
                    }
                    #endregion

                    #region Update News   
                    bool hasUpdate = false;
                    if (model.Name != entityChannel.Name)
                    {
                        entityChannel.Name = model.Name;
                        hasUpdate = true;
                    }
                    if (model.Description != entityChannel.Description)
                    {
                        entityChannel.Description = model.Description;
                        hasUpdate = true;
                    }

                    if (hasUpdate)
                    {
                        entityChannel.UpdatedDate = DateTime.Now;
                        db.SaveChanges();
                        result = new GenericResponseModel();
                        result.Success = true;
                    }
                    #endregion
                }
                #endregion
            }

            return result;
        }

        public GenericResponseModel DeleteChannel(int id)
        {
            GenericResponseModel result = new GenericResponseModel();
            if (id == 0)
            {
                throw new Exception("Id is required");
            }

            using (var db = new CDBContext())
            {
                var channel = db.Channel.Where(e => e.Status == CommonStatus.Active && e.ID == id).Select(e => e).FirstOrDefault();
                if (channel == null)
                {
                    throw new Exception("Record not found");
                }
                channel.Status = CommonStatus.Deleted;
                var query = db.ChannelNews.Where(e => e.ChannelID == id);
                db.ChannelNews.RemoveRange(query);
                db.SaveChanges();
                result = new GenericResponseModel();
                result.Success = true;
                return result;
            }
        }

    }
}