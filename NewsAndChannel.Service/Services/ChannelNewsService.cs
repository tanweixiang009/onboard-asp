﻿using NewsAndChannel.Repository;
using NewsAndChannel.Repository.Models;
using NewsAndChannel.Service.Generic;
using NewsAndChannel.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NewsAndChannel.Service.Services
{
    public class ChannelNewsService
    {

        public GeneralListingResponseModel<ChannelModel> GetSelectedChannelListByNewsId(int id)
        {
            GeneralListingResponseModel<ChannelModel> result = new GeneralListingResponseModel<ChannelModel>();
            if (id == 0)
            {
                throw new Exception("Id is required");
            }
            using (var db = new CDBContext())
            {
                var query = (from channel in db.Channel
                             join channelNews in db.ChannelNews on channel.ID equals channelNews.ChannelID
                             where id == channelNews.NewsID
                             select new ChannelModel()
                             {
                                 ID = channel.ID,
                                 Name = channel.Name,
                                 Description = channel.Description,
                                 CreatedDate = channel.CreatedDate,
                                 UpdatedDate = channel.UpdatedDate

                             });
                result.List = query.ToList();
                result.TotalCount = query.Count();
                return result;
            }

        }

        public GeneralListingResponseModel<ChannelModel> GetUnSelectedChannelListByNewsId(int id)
        {
            GeneralListingResponseModel<ChannelModel> result = new GeneralListingResponseModel<ChannelModel>();
            if (id == 0)
            {
                throw new Exception("Id is required");
            }
            using (var db = new CDBContext())
            {
                var query = (from channel in db.Channel
                             join channelNews in db.ChannelNews on channel.ID equals channelNews.ChannelID
                             where id == channelNews.NewsID
                             select new ChannelModel()
                             {
                                 ID = channel.ID,
                                 Name = channel.Name,
                                 Description = channel.Description,
                                 CreatedDate = channel.CreatedDate,
                                 UpdatedDate = channel.UpdatedDate

                             });


                var newQuery = db.Channel.Where(channel => !query.Any(e => e.ID == channel.ID)
                && channel.Status == CommonStatus.Active).
                Select(cm => new ChannelModel()
                {
                    ID = cm.ID,
                    Name = cm.Name,
                    Description = cm.Description,
                    CreatedDate = cm.CreatedDate,
                    UpdatedDate = cm.UpdatedDate
                });

                result.List = newQuery.ToList();
                result.TotalCount = newQuery.Count();

                return result;
            }

        }

        public GeneralListingResponseModel<NewsModel> GetSelectedNewsListByChannelId(int id)
        {
            GeneralListingResponseModel<NewsModel> result = new GeneralListingResponseModel<NewsModel>();
            if (id == 0)
            {
                throw new Exception("Id is required");
            }
            using (var db = new CDBContext())
            {
                var query = (from news in db.News
                             join channelNews in db.ChannelNews on news.ID equals channelNews.NewsID
                             where id == channelNews.ChannelID
                             select new NewsModel()
                             {
                                 ID = news.ID,
                                 Title = news.Title,
                                 Content = news.Content,
                                 CreatedDate = news.CreatedDate,
                                 UpdatedDate = news.UpdatedDate

                             });
                result.List = query.ToList();
                result.TotalCount = query.Count();

                return result;
            }

        }

        public GeneralListingResponseModel<NewsModel> GetUnSelectedNewsListByChannelId(int id)
        {
            GeneralListingResponseModel<NewsModel> result = new GeneralListingResponseModel<NewsModel>();
            if (id == 0)
            {
                throw new Exception("Id is required");
            }
            using (var db = new CDBContext())
            {
                var query = (from news in db.News
                             join channelNews in db.ChannelNews on news.ID equals channelNews.NewsID
                             where id == channelNews.ChannelID
                             select new NewsModel()
                             {
                                 ID = news.ID,
                                 Title = news.Title,
                                 Content = news.Content,
                                 CreatedDate = news.CreatedDate,
                                 UpdatedDate = news.UpdatedDate

                             });

                var newQuery = db.News.Where(news => !query.Any(e => e.ID == news.ID)
                && news.Status == CommonStatus.Active).
                Select(nm => new NewsModel()
                {
                    ID = nm.ID,
                    Title = nm.Title,
                    Content = nm.Content,
                    CreatedDate = nm.CreatedDate,
                    UpdatedDate = nm.UpdatedDate
                });

                result.List = newQuery.ToList();
                result.TotalCount = newQuery.Count();

                return result;
            }

        }

        public GenericResponseModel SubmitChannelNews(ChannelNewsModel model)
        {
            GenericResponseModel result = new GenericResponseModel();
            #region Add New Channel News

            using (var db = new CDBContext())

            {
                #region null-handle    
                if (model.ChannelID == 0)
                {
                    throw new Exception("Invalid Channel ID");
                }

                if (model.NewsID == 0)
                {
                    throw new Exception("Invalid News ID");
                }
                #endregion

                var channelNews = new ChannelNews()
                {
                    ChannelID = model.ChannelID,
                    NewsID = model.NewsID
                };
                db.ChannelNews.Add(channelNews);
                db.SaveChanges();
                result = new GenericResponseModel();
                result.Success = true;
            }
            #endregion
            return result;
        }

        public GenericResponseModel SubmitChannelNewsList(List<ChannelNewsModel> model)
        {
            GenericResponseModel result = new GenericResponseModel();
            #region Add New Channel News

            using (var db = new CDBContext())

            {
                if (model.Count() < 1)
                {
                    throw new Exception("At least 1 channel must be selected");
                }

                #region null-handle    
                if (model.Any(e => e.ChannelID == 0))
                {
                    throw new Exception("Invalid Channel ID");
                }

                if (model.Any(e => e.NewsID == 0))
                {
                    throw new Exception("Invalid News ID");
                }
                #endregion

                var channelNewsList = model.Select(e => new ChannelNews()
                {
                    ChannelID = e.ChannelID,
                    NewsID = e.NewsID
                });
                db.ChannelNews.AddRange(channelNewsList);
                db.SaveChanges();
                result = new GenericResponseModel();
                result.Success = true;
            }
            #endregion
            return result;
        }

        public GenericResponseModel DeleteChannelNews(ChannelNewsModel model)
        {
            GenericResponseModel result = new GenericResponseModel();
            #region Delete Channel News
            using (var db = new CDBContext())
            {
                #region null-handle    
                if (model.ChannelID == 0)
                {
                    throw new Exception("Channel ID is required");
                }
                if (model.NewsID == 0)
                {
                    throw new Exception("News ID is required");
                }
                #endregion

                #region Delete
                var cn = db.ChannelNews.Where(e => e.NewsID == model.NewsID && e.ChannelID == model.ChannelID).FirstOrDefault();
                if (cn == null)
                {
                    throw new Exception("Record not found");
                }
                db.ChannelNews.Remove(cn);
                db.SaveChanges();
                result = new GenericResponseModel();
                result.Success = true;
                #endregion

            }
            #endregion
            return result;
        }
    }
}