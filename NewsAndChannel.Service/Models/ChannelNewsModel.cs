﻿using NewsAndChannel.Repository.Models;

namespace NewsAndChannel.Service.Models
{
    public class ChannelNewsModel
    {
        public int ID { get; set; }
        public int ChannelID { get; set; }
        public int NewsID { get; set; }


        public ChannelNewsModel() { }
        public ChannelNewsModel(ChannelNews entityChannelNews)
        {
            ID = entityChannelNews.ID;
            ChannelID = entityChannelNews.ChannelID;
            NewsID = entityChannelNews.NewsID;
        }
    }
}