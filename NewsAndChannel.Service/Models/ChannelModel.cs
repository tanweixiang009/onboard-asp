﻿using NewsAndChannel.Repository.Models;
using System;

namespace NewsAndChannel.Service.Models
{
    public class ChannelModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public CommonStatus Status { get; set; }

        public ChannelModel() { }
        public ChannelModel(Channel entityChannel)
        {
            ID = entityChannel.ID;
            Name = entityChannel.Name;
            Description = entityChannel.Description;
            UpdatedDate = entityChannel.UpdatedDate;
            Status = entityChannel.Status;
            CreatedDate = entityChannel.CreatedDate;
        }
    }
}