﻿using NewsAndChannel.Repository.Models;
using System;

namespace NewsAndChannel.Service.Models
{
    public class NewsModel
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public CommonStatus Status { get; set; }

        public NewsModel() { }
        public NewsModel(News entityNews)
        {
            ID = entityNews.ID;
            Title = entityNews.Title;
            Content = entityNews.Content;
            CreatedDate = entityNews.CreatedDate;
            UpdatedDate = entityNews.UpdatedDate;
            Status = entityNews.Status;
        }
    }
}