﻿using NewsAndChannel.Repository.Models;
using NewsAndChannel.Service.Generic;
using NewsAndChannel.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Models
{
    public class NewsViewModel
    {

        public GeneralListingResponseModel<NewsModel> responseModel { get; set; }

        public GeneralListingRequestModel requestModel { get; set; }
        public News news { get; set; }
        public List<Channel> channelList { get; set; }      
        public int totalPage { get; set; }
             
    }
}