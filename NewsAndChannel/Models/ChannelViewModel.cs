﻿using NewsAndChannel.Repository.Models;
using NewsAndChannel.Service.Generic;
using NewsAndChannel.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsAndChannel.Models
{
    public class ChannelViewModel
    {
        public Channel channel { get; set; }
        public List<News> newsList { get; set; }

        public GeneralListingResponseModel<ChannelModel> responseModel { get; set; }

        public GeneralListingRequestModel requestModel { get; set; }


        public int totalPage { get; set; }

      
    }
}