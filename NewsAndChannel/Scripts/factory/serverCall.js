﻿var app = angular.module('myApp');
const baseApiUri = 'http://news-and-channel.api';

//General
//For loading icon
app.factory('Page', function () {
    var loader = false;
    var circleLoader = false;

    return {
        getLoader: function () { return loader; },
        setLoader: function (newBool) { loader = newBool; }
    };
});

//Get MVC URL parameters : Eg: http://domain/details/10. Get the parameters '10'. Only 1 parameters allow for this method.
app.factory('GetURLParameter', function () {

    return {
        get: function () {
            var sPageURL = window.location.href;
            var indexOfLastSlash = sPageURL.lastIndexOf("/");

            if (indexOfLastSlash > 0 && sPageURL.length - 1 !== indexOfLastSlash)
                return sPageURL.substring(indexOfLastSlash + 1);
            else
                return 0;
        }
    };


});
//End of General

//News Module
app.factory('getNewsList', function ($http) {
    var getData = function (filter) {
        return $http({
            method: "POST",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/news/v1/getNewsList",
            data: filter
        }).then(function (result) {
            console.log(result.data);
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('getNews', function ($http) {
    var getData = function (Id) {
        return $http({
            method: "GET",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/news/v1/getNews?NewsId=" + Id
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('getChannel', function ($http) {
    var getData = function (Id) {
        return $http({
            method: "GET",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/Channel/v1/getChannel?ChannelId=" + Id
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('getChannelList', function ($http) {
    var getData = function (filter) {
        return $http({
            method: "POST",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/Channel/v1/getChannelList",
            data: filter
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('submitNews', function ($http) {
    var getData = function (data) {
        return $http({
            method: "POST",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/news/v1/submitNews",
            data: data
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('submitChannel', function ($http) {
    var getData = function (data) {
        return $http({
            method: "POST",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/Channel/v1/submitChannel",
            data: data
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('deleteNews', function ($http) {
    var getData = function (data) {
        return $http({
            method: "GET",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/news/v1/deleteNews?NewsId=" + data.NewsId,
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('deleteChannel', function ($http) {
    var getData = function (data) {
        return $http({
            method: "GET",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/Channel/v1/deleteChannel?ChannelId=" + data.ChannelId,
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});


app.factory('getSelectedChannelListByNewsId', function ($http) {
    var getData = function (Id) {
        return $http({
            method: "GET",
            headers: {
                'X-Requested-With': "XMLHttpRequest",
            },
            AccessAllowControlOrigin: "*",
            url: baseApiUri + "/api/ChannelNews/v1/getSelectedChannelListByNewsId?NewsId=" + Id,
        }).then(function (result) {
            return result.data;
        });
    };

    return { getData: getData };
});

app.factory('getSelectedNewsListByChannelId', function ($http) {
    var getData = function (Id) {
        return $http({
            method: "GET",
            headers: {
                'X-Requested-With': "XMLHttpRequest",
            },
            AccessAllowControlOrigin: "*",
            url: baseApiUri + "/api/ChannelNews/v1/getSelectedNewsListByChannelId?ChannelId=" + Id,
        }).then(function (result) {
            return result.data;
        });
    };

    return { getData: getData };
});


app.factory('submitChannelNews', function ($http) {
    var getData = function (data) {
        return $http({
            method: "POST",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/ChannelNews/v1/submitChannelNews",
            data: data
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});


app.factory('submitChannelNewsList', function ($http) {
    var getData = function (data) {
        return $http({
            method: "POST",
            headers: {
                'X-Requested-With': "XMLHttpRequest"
            },
            url: baseApiUri + "/api/ChannelNews/v1/submitChannelNewsList",
            data: data
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});

app.factory('deleteChannelNews', function ($http) {
    var getData = function (data) {
        return $http({
            method: "DELETE",
            headers: {
                'X-Requested-With': "XMLHttpRequest",
                'Content-Type': 'application/json'
            },
            url: baseApiUri + "/api/ChannelNews/v1/deleteChannelNews",
            data: data
        }).then(function (result) {
            return result.data;
        });
    };
    return { getData: getData };
});
//End of News Module
