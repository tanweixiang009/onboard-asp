﻿var app = angular.module('myApp');

app.controller('ListController', function (Page, $q, $http, getNewsList, deleteNews) {
    var vm = this;

    /* Start declare */
    vm.List = [];

    //Listing
    /* Start declare */
    vm.page = 1;  //Regardless 1-based or zero-based for page index, also set 1 for first page
    vm.pageSize = 3;
    vm.totalrecord = 0;
    vm.searchKeyword = null;
    vm.totalCount = 0;
    /*End of Filter Drop Down*/

    //Default sorting column and sorting direction
    vm.sortStatus = {
        column: 'createddate',
        orderDir: 'desc'
    };

    //Table Columns
    vm.tableColumn = [{
        Name: 'No.',
        SortingField: null,
        EnableSorting: false
    }, {
        Name: 'Title',
        SortingField: 'title',
        EnableSorting: true
    }, {
        Name: 'Content',
        SortingField: 'content',
        EnableSorting: true
    }, {
        Name: 'Created Date',
        SortingField: 'createddate',
        EnableSorting: true
    }, {
        Name: 'Updated Date',
        SortingField: 'updateddate',
        EnableSorting: true
    }, {
        Name: 'Action',
        SortingField: null,
        EnableSorting: false
    }
    ];
    /* End declare */

    /* Start function */
    vm.getList = function () {
        Page.setLoader(true);
        var d = $q.defer();
        var filter = {
            OrderDir: vm.sortStatus.orderDir,
            SortColumn: vm.sortStatus.column,
            PageIndex: vm.page - 1,  // Due to Zero-based Page Index
            PageSize: vm.pageSize,
            SearchKeyword: vm.searchKeyword
        };

        getNewsList.getData(filter).then(function (response) {
            d.resolve(response);
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });

        return d.promise;
    };

    //Page sorting, shift page, search
    vm.refreshList = function () {
        vm.getList().then(function (response) {
            vm.List = response.List;
            vm.totalCount = response.TotalCount;
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });
    };

    vm.search = function (event) {
        if (event && (event.keyCode === 13 || event.type === 'mouseup' || !vm.searchKeyword)) {
            vm.refreshList();
        }
    };

    vm.sort = function (enableSorting, columnName) {
        if (enableSorting) {
            if (columnName === vm.sortStatus.column) {
                if (vm.sortStatus.orderDir === 'asc') {
                    vm.sortStatus.orderDir = 'desc';
                } else {
                    vm.sortStatus.orderDir = 'asc';
                }
            } else {
                vm.sortStatus.orderDir = 'asc';
            }

            vm.sortStatus.column = columnName;

            vm.refreshList();
        }
    };

    vm.sortIcon = function (enableSorting, data) {
        if (enableSorting) {
            if (data === vm.sortStatus.column) {
                if (vm.sortStatus.orderDir === 'asc') {
                    return "icon-sort-asc";
                } else if (vm.sortStatus.orderDir === 'desc') {
                    return "icon-sort-desc";
                }
            }
            return "icon-sort";
        }
        return;
    };

    vm.remove = function (rec) {
        if (confirm("Are you sure want to delete?")) {
            Page.setLoader(true);
            var d = $q.defer();
            var data = {
                NewsId: parseInt(rec.ID)
            }
            deleteNews.getData(data).then(function (response) {
                d.resolve(response);
                if (response.Success !== null && response.Success !== undefined) {
                    if (response.Success) {
                        window.location.href = "/News";
                        alert("News Deleted !");

                    }
                }
                if (response.status !== null && response.status !== undefined) {
                    if (response.status.code !== 200) {
                        alert(response.status.message);
                    }
                }

            }, function (resp) {
                alert('Error status: ' + resp.data.error);
                return;
            }).finally(function () {
                Page.setLoader(false);
            });

            return d.promise;
        }

    };


    //------------------------------------//

    /* Start Run */
    $q.all([
        vm.getList()
    ]).then(function (data) {

        vm.List = data[0].List;
        vm.totalCount = data[0].TotalCount;

        Page.setLoader(false);

    });
    /* End Run */
});

