﻿

var app = angular.module('myApp');

app.controller('EditController', function (Page, $q, getNews, getChannelList,
    getSelectedChannelListByNewsId, submitNews, GetURLParameter, deleteNews,
    submitChannelNews, deleteChannelNews) {

    var vm = this;
    vm.Id = GetURLParameter.get();
    vm.record = null;
    vm.ChannelList = [];
    vm.SelectedChannelList = [];

    ///* Start function */
    vm.getDetails = function (Id) {
        Page.setLoader(true);
        var d = $q.defer();

        getNews.getData(Id).then(function (response) {
            d.resolve(response);
        },
            function (resp) {
                Page.setLoader(false);
                alert('Error status: ' + resp.data.error);
                return;
            });

        return d.promise;
    };

    vm.getChannelList = function () {
        Page.setLoader(true);
        var d = $q.defer();
        var filter = {
            OrderDir: 'asc',
            SortColumn: 'name',
            PageIndex: 0,
            PageSize: 100,
            SearchKeyword: null
        };

        getChannelList.getData(filter).then(function (response) {
            d.resolve(response);
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });

        return d.promise;
    };

    vm.getSelectedChannelList = function (Id) {
        Page.setLoader(true);
        var d = $q.defer();

        getSelectedChannelListByNewsId.getData(Id).then(function (response) {
            d.resolve(response);
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });

        return d.promise;
    };

    //-----Category multiple selection ----
    vm.hideAllList = function () {

        vm.showChannelList = false;
        vm.searchChannel = "";

    };

    vm.showChannelList = false;

    vm.searchChannel = "";

    vm.openChannelList = function () {
        vm.showChannelList = true;
    };

    vm.removeChannel = function (rec) {
        if (vm.SelectedChannelList.length == 1) {
            alert("Cannot remove because At least must has 1 channel");
            return
        }

        Page.setLoader(true);
        vm.hideAllList();

        var recFind = vm.ChannelList.find(e => e.ID === rec.ID);
        if (recFind) {

            var d = $q.defer();
            var data = {
                NewsID: parseInt(vm.Id),
                ChannelID: recFind.ID
            }
            deleteChannelNews.getData(data).then(function (response) {
                d.resolve(response);
                if (response.Success !== null && response.Success !== undefined) {
                    if (response.Success) {
                        alert("Channel News Deleted !");
                        recFind.selected = false;
                        vm.searchChannel = "";
                        vm.SelectedChannelList.splice(vm.SelectedChannelList.indexOf(rec), 1);
                    }
                }

                if (response.status !== null && response.status !== undefined) {
                    if (response.status.code !== 200) {
                        alert(response.status.message);
                    }
                }

            }, function (resp) {
                alert('Error status: ' + resp.data.error);
                return;
            }).finally(function () {
                Page.setLoader(false);
            });
            return d.promise;

        }

    };

    vm.refreshChannelList = function () {
        if (vm.searchChannel !== null) {
            if (vm.searchChannel.length >= 3 || vm.searchChannel.length === 0) {
                vm.getChannelList().then(function (response) {
                    var data = [];
                    //filter the result based on the search keyword
                    response.List.map(function (e) {
                        console.log(e.Name.startsWith(vm.searchChannel));
                        if (e.Name.startsWith(vm.searchChannel)) {
                            data.push(e);
                        }
                    });

                    //loop through the selected list and set visible for the selected list
                    vm.SelectedChannelList.forEach(function (rec) {
                        var recFind = data.find(e => e.ID === rec.ID);
                        if (recFind) {
                            recFind.selected = true;
                        }
                    });
                    vm.ChannelList = data;
                });
            }
        }

    };


    vm.updateChannelList = function (rec) {
        Page.setLoader(true);
        var d = $q.defer();
        var data = {
            NewsID: parseInt(vm.Id),
            ChannelID: rec.ID
        }
        submitChannelNews.getData(data).then(function (response) {
            d.resolve(response);
            if (response.Success !== null && response.Success !== undefined) {
                if (response.Success) {
                    alert("Channel News Added !");
                    rec.selected = true;
                    vm.SelectedChannelList.push(rec);
                }
            }
            if (response.status !== null && response.status !== undefined) {
                if (response.status.code !== 200) {
                    alert(response.status.message);
                }
            }

        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);

        });

        return d.promise;

    };
    //-----End of Category multiple selection ----


    vm.Submit = function () {

        Page.setLoader(true);

        //validation
        if (vm.record.Title === '' || vm.record.Title === null || vm.record.Title === undefined) {
            alert("Title is mandatory");
            Page.setLoader(false);
            return;
        }
        if (vm.record.Content === '' || vm.record.Content === null || vm.record.Content === undefined) {
            alert("Content is mandatory");
            Page.setLoader(false);
            return;
        }

        //Submission
        var d = $q.defer();
        submitNews.getData(vm.record).then(function (response) {
            d.resolve(response);
            if (response.Success !== null && response.Success !== undefined) {
                if (response.Success) {
                    alert('Update Success');

                }
            }
            if (response.status !== null && response.status !== undefined) {
                if (response.status.code !== 200) {
                    alert(response.status.message);
                }
            }
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).catch(function (error) {
            console.error(error);
        }).finally(function () {
            Page.setLoader(false);

        });

        return d.promise;


    };

    vm.Delete = function () {
        if (confirm("Are you sure want to delete?")) {
            Page.setLoader(true);
            var d = $q.defer();
            var data = {
                NewsId: parseInt(vm.Id)
            }

            deleteNews.getData(data).then(function (response) {
                d.resolve(response);
                if (response.Success !== null && response.Success !== undefined) {
                    if (response.Success) {
                        alert("News Deleted !");
                        window.location.href = "/News";

                    }
                }
                if (response.status !== null && response.status !== undefined) {
                    if (response.status.code !== 200) {
                        alert(response.status.message);
                    }
                }

            }, function (resp) {
                alert('Error status: ' + resp.data.error);
                return;
            }).finally(function () {
                Page.setLoader(false);
            });

            return d.promise;
        }
    };

    //------------------------------------//

    /* Start Run */
    $q.all([
        vm.getChannelList(),
        vm.getDetails(vm.Id),
        vm.getSelectedChannelList(vm.Id)
    ]).then(function (data) {
        vm.ChannelList = data[0].List;
        vm.record = data[1];
        vm.SelectedChannelList = data[2].List;

        vm.SelectedChannelList.forEach(function (rec) {
            var recFind = vm.ChannelList.find(e => e.ID === rec.ID);
            if (recFind) {
                recFind.selected = true;
            }
        });
        Page.setLoader(false);

    });
    /* End Run */


});

