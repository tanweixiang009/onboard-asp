﻿
var app = angular.module('myApp');

app.controller('CreateController', function (Page, $q, getChannelList,
    submitNews, submitChannelNewsList) {

    var vm = this;
    vm.record = null;
    vm.ChannelList = [];
    vm.SelectedChannelList = [];

    ///* Start function */

    vm.getChannelList = function () {
        Page.setLoader(true);
        var d = $q.defer();
        var filter = {
            OrderDir: 'asc',
            SortColumn: 'name',
            PageIndex: 0,
            PageSize: 100,
            SearchKeyword: null
        };

        getChannelList.getData(filter).then(function (response) {
            d.resolve(response);
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });

        return d.promise;
    };


    //-----Category multiple selection ----
    vm.hideAllList = function () {

        vm.showChannelList = false;
        vm.searchChannel = "";

    };

    vm.showChannelList = false;

    vm.searchChannel = "";

    vm.openChannelList = function () {
        vm.showChannelList = true;
    };

    vm.removeChannel = function (rec) {
        Page.setLoader(true);
        vm.hideAllList();

        var recFind = vm.ChannelList.find(e => e.ID === rec.ID);

        if (recFind) {
            recFind.selected = false;
            vm.searchChannel = "";
            vm.SelectedChannelList.splice(vm.SelectedChannelList.indexOf(rec), 1);
        }
        Page.setLoader(false);

    };

    vm.refreshChannelList = function () {
        if (vm.searchChannel !== null) {
            if (vm.searchChannel.length >= 3 || vm.searchChannel.length === 0) {
                vm.getChannelList().then(function (response) {
                    var data = [];
                    //filter the result based on the search keyword
                    response.List.map(function (e) {
                        console.log(e.Name.startsWith(vm.searchChannel));
                        if (e.Name.startsWith(vm.searchChannel)) {
                            data.push(e);
                        }
                    });

                    //loop through the selected list and set visible for the selected list
                    vm.SelectedChannelList.forEach(function (rec) {
                        var recFind = data.find(e => e.ID === rec.ID);
                        if (recFind) {
                            recFind.selected = true;
                        }
                    });
                    vm.ChannelList = data;
                });
            }
        }

    };


    vm.updateChannelList = function (rec) {
        Page.setLoader(true);
        rec.selected = true;
        vm.SelectedChannelList.push(rec);
        Page.setLoader(false);

    };
    //-----End of Category multiple selection ----


    vm.Submit = function () {

        Page.setLoader(true);
        if (vm.record == null) {
            alert("Title and Content is mandatory");
            Page.setLoader(false);
            return;
        }
        //validation
        if (vm.record.Title == null) {
            alert("Title is mandatory");
            Page.setLoader(false);
            return;
        }
        if (vm.record.Content === '' || vm.record.Content === null || vm.record.Content === undefined) {
            alert("Content is mandatory");
            Page.setLoader(false);
            return;
        }
        if (vm.SelectedChannelList.length == 0) {
            alert("At least 1 channel must be selected");
            Page.setLoader(false);
            return;
        }

        //Submission
        var d = $q.defer();
        submitNews.getData(vm.record).then(function (response) {
            d.resolve(response);
            if (response.Success !== null && response.Success !== undefined) {
                if (response.Success) {
                    var data = [];
                    vm.SelectedChannelList.forEach(function (e) {
                        data.push({
                            NewsID: parseInt(response.Message),
                            ChannelID: e.ID
                        })
                    });
                    submitChannelNewsList.getData(data).then(function (response) {
                        if (response.Success !== null && response.Success !== undefined) {
                            if (response.Success) {
                                alert('Create Success');
                                window.location = "/News";
                            }
                            else {
                                alert(response);
                            }
                        }
                    });
                }
                else {
                    alert(response);
                }
            }
            if (response.status !== null && response.status !== undefined) {
                if (response.status.code !== 200) {
                    alert(response.status.message);
                }
            }
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).catch(function (error) {
            console.error(error);
        }).finally(function () {
            Page.setLoader(false);

        });

        return d.promise;


    };

    //------------------------------------//

    /* Start Run */
    $q.all([
        vm.getChannelList(),
    ]).then(function (data) {
        vm.ChannelList = data[0].List;
        Page.setLoader(false);

    });
    /* End Run */


});

