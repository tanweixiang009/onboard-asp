﻿
var app = angular.module('myApp');

app.controller('CreateController', function (Page, $q, getNewsList,
    submitChannel, submitChannelNewsList) {

    var vm = this;
    vm.record = null;
    vm.NewsList = [];
    vm.SelectedNewsList = [];

    ///* Start function */

    vm.getNewsList = function () {
        Page.setLoader(true);
        var d = $q.defer();
        var filter = {
            OrderDir: 'asc',
            SortColumn: 'name',
            PageIndex: 0,
            PageSize: 100,
            SearchKeyword: null
        };

        getNewsList.getData(filter).then(function (response) {
            d.resolve(response);
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });

        return d.promise;
    };


    //-----Category multiple selection ----
    vm.hideAllList = function () {

        vm.showNewsList = false;
        vm.searchNews = "";

    };

    vm.showNewsList = false;

    vm.searchNews = "";

    vm.openNewsList = function () {
        vm.showNewsList = true;
    };

    vm.removeNews = function (rec) {
        Page.setLoader(true);
        vm.hideAllList();

        var recFind = vm.NewsList.find(e => e.ID === rec.ID);

        if (recFind) {
            recFind.selected = false;
            vm.searchNews = "";
            vm.SelectedNewsList.splice(vm.SelectedNewsList.indexOf(rec), 1);
        }
        Page.setLoader(false);

    };

    vm.refreshNewsList = function () {
        if (vm.searchNews !== null) {
            if (vm.searchNews.length >= 3 || vm.searchNews.length === 0) {
                vm.getNewsList().then(function (response) {
                    var data = [];
                    //filter the result based on the search keyword
                    response.List.map(function (e) {
                        if (e.Title.startsWith(vm.searchNews)) {
                            data.push(e);
                        }
                    });

                    //loop through the selected list and set visible for the selected list
                    vm.SelectedNewsList.forEach(function (rec) {
                        var recFind = data.find(e => e.ID === rec.ID);
                        if (recFind) {
                            recFind.selected = true;
                        }
                    });
                    vm.NewsList = data;
                });
            }
        }

    };


    vm.updateNewsList = function (rec) {
        Page.setLoader(true);
        rec.selected = true;
        vm.SelectedNewsList.push(rec);
        Page.setLoader(false);

    };
    //-----End of Category multiple selection ----


    vm.Submit = function () {

        Page.setLoader(true);
        //validation
        if (vm.record == null) {
            alert("Name and Description are mandatory");
            Page.setLoader(false);
            return;
        }

        if (vm.record.Name == null || vm.record.Name === '' || vm.record.Name === undefined) {
            alert("Name is mandatory");
            Page.setLoader(false);
            return;
        }
        if (vm.record.Description === '' || vm.record.Description === null || vm.record.Description === undefined) {
            alert("Description is mandatory");
            Page.setLoader(false);
            return;
        }

        //Submission
        var d = $q.defer();
        submitChannel.getData(vm.record).then(function (response) {
            d.resolve(response);
            if (response.Success !== null && response.Success !== undefined) {
                if (response.Success) {
                    if (vm.SelectedNewsList.length > 0) {
                        var data = [];
                        vm.SelectedNewsList.forEach(function (e) {
                            data.push({
                                NewsID: e.ID,
                                ChannelID: parseInt(response.Message)
                            })
                        });
                        submitChannelNewsList.getData(data).then(function (response) {
                            if (response.Success !== null && response.Success !== undefined) {
                                if (response.Success) {
                                    alert('Create Success');
                                    window.location = "/Channel";
                                }
                                else {
                                    alert(response);
                                }
                            }
                            if (response.status !== null && response.status !== undefined) {
                                if (response.status.code !== 200) {
                                    alert(response.status.message);
                                }
                            }
                        });
                    }
                    else {
                        alert('Create Success');
                        window.location = "/Channel";
                    }

                }
                else {
                    alert(response);
                }
            }
            if (response.status !== null && response.status !== undefined) {
                if (response.status.code !== 200) {
                    alert(response.status.message);
                }
            }
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).catch(function (error) {
            console.error(error);
        }).finally(function () {
            Page.setLoader(false);

        });

        return d.promise;


    };

    //------------------------------------//

    /* Start Run */
    $q.all([
        vm.getNewsList(),
    ]).then(function (data) {
        vm.NewsList = data[0].List;
        Page.setLoader(false);

    });
    /* End Run */


});

