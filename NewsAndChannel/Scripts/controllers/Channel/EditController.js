﻿

var app = angular.module('myApp');

app.controller('EditController', function (Page, $q, getChannel, getNewsList,
    getSelectedNewsListByChannelId, submitChannel, GetURLParameter, deleteChannel,
    submitChannelNews, deleteChannelNews) {

    var vm = this;
    vm.Id = GetURLParameter.get();
    vm.record = null;
    vm.NewsList = [];
    vm.SelectedNewsList = [];

    ///* Start function */
    vm.getDetails = function (Id) {
        Page.setLoader(true);
        var d = $q.defer();

        getChannel.getData(Id).then(function (response) {
            d.resolve(response);
        },
            function (resp) {
                Page.setLoader(false);
                alert('Error status: ' + resp.data.error);
                return;
            });

        return d.promise;
    };

    vm.getNewsList = function () {
        Page.setLoader(true);
        var d = $q.defer();
        var filter = {
            OrderDir: 'asc',
            SortColumn: 'title',
            PageIndex: 0,
            PageSize: 100,
            SearchKeyword: null
        };

        getNewsList.getData(filter).then(function (response) {
            d.resolve(response);
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });

        return d.promise;
    };

    vm.getSelectedNewsList = function (Id) {
        Page.setLoader(true);
        var d = $q.defer();

        getSelectedNewsListByChannelId.getData(Id).then(function (response) {
            d.resolve(response);
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);
        });

        return d.promise;
    };

    //-----Category multiple selection ----
    vm.hideAllList = function () {

        vm.showNewsList = false;
        vm.searchNews = "";

    };

    vm.showNewsList = false;

    vm.searchNews = "";

    vm.openNewsList = function () {
        vm.showNewsList = true;
    };

    vm.removeNews = function (rec) {

        Page.setLoader(true);
        vm.hideAllList();

        var recFind = vm.NewsList.find(e => e.ID === rec.ID);
        if (recFind) {

            var d = $q.defer();
            var data = {
                NewsID: recFind.ID,
                ChannelID: parseInt(vm.Id)
            }
            deleteChannelNews.getData(data).then(function (response) {
                d.resolve(response);
                if (response.Success !== null && response.Success !== undefined) {
                    if (response.Success) {
                        alert("Channel News Deleted !");
                        recFind.selected = false;
                        vm.searchNews = "";
                        vm.SelectedNewsList.splice(vm.SelectedNewsList.indexOf(rec), 1);
                    }
                }

                if (response.status !== null && response.status !== undefined) {
                    if (response.status.code !== 200) {
                        alert(response.status.message);
                    }
                }

            }, function (resp) {
                alert('Error status: ' + resp.data.error);
                return;
            }).finally(function () {
                Page.setLoader(false);
            });
            return d.promise;

        }

    };

    vm.refreshNewsList = function () {
        if (vm.searchNews !== null) {
            if (vm.searchNews.length >= 3 || vm.searchNews.length === 0) {
                vm.getNewsList().then(function (response) {
                    var data = [];
                    //filter the result based on the search keyword
                    response.List.map(function (e) {
                        if (e.Title.startsWith(vm.searchNews)) {
                            data.push(e);
                        }
                    });

                    //loop through the selected list and set visible for the selected list
                    vm.SelectedNewsList.forEach(function (rec) {
                        var recFind = data.find(e => e.ID === rec.ID);
                        if (recFind) {
                            recFind.selected = true;
                        }
                    });
                    vm.NewsList = data;
                });
            }
        }

    };


    vm.updateNewsList = function (rec) {
        Page.setLoader(true);
        var d = $q.defer();
        var data = {
            NewsID: rec.ID,
            ChannelID: parseInt(vm.Id)
        }
        submitChannelNews.getData(data).then(function (response) {
            d.resolve(response);
            if (response.Success !== null && response.Success !== undefined) {
                if (response.Success) {
                    alert("Channel News Added !");
                    rec.selected = true;
                    vm.SelectedNewsList.push(rec);
                }
            }
            if (response.status !== null && response.status !== undefined) {
                if (response.status.code !== 200) {
                    alert(response.status.message);
                }
            }

        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).finally(function () {
            Page.setLoader(false);

        });

        return d.promise;

    };
    //-----End of Category multiple selection ----


    vm.Submit = function () {

        Page.setLoader(true);

        //validation
        if (vm.record.Name === '' || vm.record.Name === null || vm.record.Name === undefined) {
            alert("Name is mandatory");
            Page.setLoader(false);
            return;
        }
        if (vm.record.Description === '' || vm.record.Description === null || vm.record.Description === undefined) {
            alert("Description is mandatory");
            Page.setLoader(false);
            return;
        }

        //Submission
        var d = $q.defer();
        submitChannel.getData(vm.record).then(function (response) {
            d.resolve(response);
            if (response.Success !== null && response.Success !== undefined) {
                if (response.Success) {
                    alert('Update Success');

                }
            }
            if (response.status !== null && response.status !== undefined) {
                if (response.status.code !== 200) {
                    alert(response.status.message);
                }
            }
        }, function (resp) {
            alert('Error status: ' + resp.data.error);
            return;
        }).catch(function (error) {
            console.error(error);
        }).finally(function () {
            Page.setLoader(false);

        });

        return d.promise;


    };

    vm.Delete = function () {
        if (confirm("Are you sure want to delete?")) {
            Page.setLoader(true);
            var d = $q.defer();
            var data = {
                ChannelId: parseInt(vm.Id)
            }
            deleteChannel.getData(data).then(function (response) {
                d.resolve(response);
                if (response.Success !== null && response.Success !== undefined) {
                    if (response.Success) {
                        alert("Channel Deleted !");
                        window.location.href = "/Channel";

                    }
                }
                if (response.status !== null && response.status !== undefined) {
                    if (response.status.code !== 200) {
                        alert(response.status.message);
                    }
                }

            }, function (resp) {
                alert('Error status: ' + resp.data.error);
                return;
            }).finally(function () {
                Page.setLoader(false);
            });

            return d.promise;
        }
    };

    //------------------------------------//

    /* Start Run */
    $q.all([
        vm.getNewsList(),
        vm.getDetails(vm.Id),
        vm.getSelectedNewsList(vm.Id)
    ]).then(function (data) {
        vm.NewsList = data[0].List;
        vm.record = data[1];
        vm.SelectedNewsList = data[2].List;

        vm.SelectedNewsList.forEach(function (rec) {
            var recFind = vm.NewsList.find(e => e.ID === rec.ID);
            if (recFind) {
                recFind.selected = true;
            }
        });
        Page.setLoader(false);

    });
    /* End Run */


});

